/**
 * tourly allows auto-launching of tours.
 */
(function ($, Backbone, Drupal, document) {
  Drupal.behaviors.tourly_autolaunch = {
    attach: function attach(context) {
      // hide the tourly button
      $('button.tourly', context).hide();
      // auto-launch tour
      $('#toolbar-tab-tour', context).trigger('click');
      // please note that the above two buttons may or may not be the same element
    }
  };
})(jQuery, Backbone, Drupal, document);
